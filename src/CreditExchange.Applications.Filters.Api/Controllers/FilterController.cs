﻿using CreditExchange.Applications.Filters.Abstractions;
using CreditExchange.Applications.Filters.Abstractions.Services;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace CreditExchange.Applications.Filters.Api.Controllers
{
    [Route("/")]
    public class FilterController : ExtendedController
    {
        public FilterController(IApplicationFilterService service)
        {
            Service = service;
        }

        private IApplicationFilterService Service { get; }

        [HttpGet("/all")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public IActionResult GetAll()
        {            
            return Execute(() => Ok(Service.GetAll()));
        }

        [HttpGet("export/all/{templatename}")]
        [ProducesResponseType(typeof(FileContentResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public IActionResult ExportAll(string templatename)
        {
            var result = Service.ExportAll(templatename);
            return new FileActionResult(result.FileDownloadName, result.FileContents, result.ContentType);
        }

        [HttpGet("/{sourceType}/{sourceId}/all")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]

        public IActionResult GetAllBySource(string sourceType, string sourceId)
        {
            return Execute(() => Ok(Service.GetAllBySource(sourceType, sourceId)));
        }

        [HttpGet("/{sourceType}/{sourceId}/metrics/submit/month")]
        [ProducesResponseType(typeof(int), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public Task<IActionResult> GetTotalSubmittedForCurrentMonth(string sourceType, string sourceId)
        {
            return ExecuteAsync(async () => Ok(await Service.GetTotalSubmittedForCurrentMonth(sourceType, sourceId)));
        }

        [HttpGet("/{sourceType}/{sourceId}/metrics/submit/year")]
        [ProducesResponseType(typeof(int), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public Task<IActionResult> GetTotalSubmittedForCurrentYear(string sourceType, string sourceId)
        {
            return ExecuteAsync(async () => Ok(await Service.GetTotalSubmittedForCurrentYear(sourceType, sourceId)));
        }

        [HttpGet("/{sourceType}/{sourceId}/status/{*statuses}")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public Task<IActionResult> GetByStatus(string sourceType, string sourceId, string statuses)
        {
            return ExecuteAsync(async () =>
            {
                statuses = WebUtility.UrlDecode(statuses);
                var listStatus = statuses?.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return Ok(await Service.GetByStatus(sourceType, sourceId, listStatus));
            });
        }

        [HttpGet("status/{*statuses}")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public Task<IActionResult> GetAllByStatus(string statuses)
        {
            return ExecuteAsync(async () =>
            {
                statuses = WebUtility.UrlDecode(statuses);
                var listStatus = statuses?.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return Ok(await Service.GetAllByStatus(listStatus));
            });
        }

        [HttpGet("export/{templatename}/status/{*statuses}")]
        [ProducesResponseType(typeof(FileContentResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> ExportAllByStatus(string templatename, string statuses)
        {
            statuses = WebUtility.UrlDecode(statuses);
            var listStatus = statuses?.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            var result = await Service.ExportAllByStatus(templatename, listStatus);
            return new FileActionResult(result.FileDownloadName, result.FileContents, result.ContentType);
        }

        [HttpGet("/{sourceType}/{sourceId}/count/status/{*statuses}")]
        [ProducesResponseType(typeof(int), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]

        public Task<IActionResult> GetCountByStatus(string sourceType, string sourceId, string statuses)
        {
            return ExecuteAsync(async () =>
            {
                statuses = WebUtility.UrlDecode(statuses);
                var listStatus = statuses?.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return Ok(await Service.GetCountByStatus(sourceType, sourceId, listStatus));
            });
        }

        [HttpGet("tag/{*tags}")]
       
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public Task<IActionResult> GetAllByTag(string tags)
        {
            return ExecuteAsync(async () =>
            {
                tags = WebUtility.UrlDecode(tags);
                var listTags = tags?.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return Ok(await Service.GetAllByTag(listTags));
            });
        }

        [HttpGet("export/{templatename}/tag/{*tags}")]
        [ProducesResponseType(typeof(FileContentResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> ExportAllByTag(string templatename, string tags)
        {
            tags = WebUtility.UrlDecode(tags);
            var listTags = tags?.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            var result = await Service.ExportAllByTag(templatename, listTags);
            return new FileActionResult(result.FileDownloadName, result.FileContents, result.ContentType);
        }

        [HttpGet("tag/taginfo/{tag}")]
        [ProducesResponseType(typeof(IEnumerable<IFilterViewTags>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public Task<IActionResult> GetTagInfoForApplicationsByTag(string tag)
        {
            return ExecuteAsync(async () =>
            {
                return Ok(await Service.GetTagInfoForApplicationsByTag(WebUtility.UrlDecode(tag)));
            });
        }

        [HttpGet("/count/tag/{*tags}")]
        [ProducesResponseType(typeof(int), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]

        public Task<IActionResult> GetCountByTag(string tags)
        {
            return ExecuteAsync(async () =>
            {
                tags = WebUtility.UrlDecode(tags);
                var listTags = tags?.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return Ok(await Service.GetCountByTag(listTags));
            });
        }

        [HttpGet("/{sourceType}/{sourceId}/search/{name}")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public Task<IActionResult> GetByName(string sourceType, string sourceId, string name)
        {
            return ExecuteAsync(async () =>
            {
                name = WebUtility.UrlDecode(name);
                return Ok(await Service.GetByName(sourceType, sourceId, name));
            });
        }

        [HttpGet("/{sourceType}/{sourceId}/metrics/total-approved-amount")]
        [ProducesResponseType(typeof(double), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public IActionResult GetTotalAmountApprovedBySource(string sourceType, string sourceId)
        {
            return Execute(() => Ok(Service.GetTotalAmountApprovedBySource(sourceType, sourceId)));
        }

        [HttpGet("/{sourceType}/{sourceId}/{applicationNumber}")]
        [ProducesResponseType(typeof(IFilterView), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public Task<IActionResult> GetByApplicationNumber(string sourceType, string sourceId, string applicationNumber)
        {
            return ExecuteAsync(async () => Ok(await Service.GetByApplicationNumber(sourceType, sourceId, applicationNumber)));
        }

        [HttpGet("getwithstatushistory/{sourceType}/{sourceId}/{applicationNumber}")]
        [ProducesResponseType(typeof(IFilterView), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public Task<IActionResult> GetByApplicationNumberWithStatusHistory(string sourceType, string sourceId, string applicationNumber)
        {
            return ExecuteAsync(async () => Ok(await Service.GetByApplicationNumberWithStatusHistory(sourceType, sourceId, applicationNumber)));
        }

        [HttpGet("duplicateexistsdata/{parametertype}/{parametervalue}")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> DuplicateExistsData(UniqueParameterTypes parametertype, string parametervalue)
        {
            try
            {
                return await ExecuteAsync(async () =>
                {
                    return Ok(await Service.DuplicateExistsData(parametertype, parametertype == UniqueParameterTypes.Email ? WebUtility.UrlDecode(parametervalue) : parametervalue));
                });
            }
            catch (Exception ex)
            {
                return new ErrorResult(500, ex.Message);
            }
        }

        [HttpGet("/expired/all")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetAllExpiredApplications()
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Service.GetAllExpiredApplications());
            });
        }

        [HttpGet("/expired/by/{*statusCode}")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetAllExpiredApplications(string statusCode)
        {
            return await ExecuteAsync(async () =>
            {
                statusCode = WebUtility.UrlDecode(statusCode);
                var listStatusCode = statusCode?.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return Ok(await Service.GetExcludedStatusExpired(listStatusCode));
            });
        }

        [HttpPost("applytags/{applicationnumber}/{*tags}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public IActionResult ApplyTagsToApplication(string applicationnumber, string tags)
        {
            try
            {
                return Execute(() =>
                {
                    tags = WebUtility.UrlDecode(tags);
                    var listTags = tags?.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                    Service.TagsWithoutEvaluation(applicationnumber, listTags);
                    return Ok();
                });
            }
            catch (ArgumentNullException ex)
            {
                return new ErrorResult(404, ex.Message);
            }
            catch (Exception ex)
            {
                return new ErrorResult(500, ex.Message);
            }
        }

        [HttpPost("removetags/{applicationnumber}/{*tags}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public IActionResult RemoveTagsFromApplication(string applicationnumber, string tags)
        {
            try
            {
                return Execute(() =>
                {
                    tags = WebUtility.UrlDecode(tags);
                    var listTags = tags?.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                    Service.UnTagWithoutEvaluation(applicationnumber, listTags);
                    return Ok();
                });
            }
            catch (ArgumentNullException ex)
            {
                return new ErrorResult(404, ex.Message);
            }
            catch (Exception ex)
            {
                return new ErrorResult(500, ex.Message);
            }
        }

        [HttpPost("searchapplications/{condition?}")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]

        public async Task<IActionResult> SearchApplications([FromBody]ApplicationSearchParams searchParams, string condition = null)
        {
            try
            {
                return await ExecuteAsync(async () =>
                 {
                     return Ok(await Service.SearchApplications(searchParams, condition));
                 });
            }
            catch (Exception ex)
            {
                return new ErrorResult(500, ex.Message);
            }
        }
        [HttpPost("searchapplications/bank/{condition?}")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> SearchApplicationsForBank([FromBody]ApplicationSearchParams searchParams, string condition = null)
        {
            try
            {
                return await ExecuteAsync(async () =>
                {
                    return Ok(await Service.SearchApplicationsForBank(searchParams, condition));
                });
            }
            catch (Exception ex)
            {
                return new ErrorResult(500, ex.Message);
            }
        }
        [HttpGet("searchforattribute/{attributename}/{attributevalue}")]
        [ProducesResponseType(typeof(Tuple<string, FilterView, FilterViewTags>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public IActionResult SearchForAttributeValue(string attributename, string attributevalue)
        {
            try
            {
                return Execute(() =>
                {
                    return Ok(Service.SearchApplicationForAttribute(attributename, WebUtility.UrlDecode(attributevalue)));
                });

            }
            catch (Exception ex)
            {
                return new ErrorResult(500, ex.Message);
            }
        }

        [HttpGet("getapplicationtaginfo/{applicationnumber}")]
        [ProducesResponseType(typeof(IFilterViewTags), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public IActionResult GetApplicationTagInfo(string applicationnumber)
        {
            try
            {
                return Execute(() =>
                {
                    return Ok(Service.GetApplicationTagInformation(applicationnumber));
                });
            }
            catch (Exception ex)
            {
                return new ErrorResult(500, ex.Message);
            }
        }

        [HttpPost("attachhunterDoc/{docmanagerentityid}/{applicationnumber}/{documentid}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public IActionResult AttachHunterDocument(string applicationnumber, string docmanagerentityid, string documentid)
        {
            try
            {
                return Execute(() =>
                {
                    Service.AttachHunterDocumentID(applicationnumber, docmanagerentityid, documentid);
                    return Ok();
                });
            }
            catch (Exception ex)
            {
                return new ErrorResult(500, ex.Message);
            }
        }

        [HttpPost("attachfinacleDoc/{docmanagerentityid}/{applicationnumber}/{documentid}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public IActionResult AttachFinacleDocument(string applicationnumber, string docmanagerentityid, string documentid)
        {
            try
            {
                return Execute(() =>
                {
                    Service.AttachFinacleDocumentID(applicationnumber, docmanagerentityid, documentid);
                    return Ok();
                });
            }
            catch (Exception ex)
            {
                return new ErrorResult(500, ex.Message);
            }
        }

        [HttpPost("filterontaggedtime/{tagname}/{fromdate}/{todate?}")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public Task<IActionResult> FilterOnTaggedTime(string tagname, string fromdate, string todate = null)
        {
            return ExecuteAsync(async () =>
            {
                return Ok(await Service.ApplicationsTaggedOn(WebUtility.UrlDecode(tagname), fromdate, todate));
            });
        }
        [HttpGet("/unassigned/all")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public Task<IActionResult> GetAllUnassigned()
        {
            return ExecuteAsync(async () => Ok(await Service.GetAllUnassignedByUser()));
        }

        [HttpGet("/mine")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public Task<IActionResult> GetAllAssignedApplications()
        {
            return ExecuteAsync(async () => Ok(await Service.GetAllAssignedApplications()));
        }

        [HttpGet("/count")]
        [ProducesResponseType(typeof(IDictionary<string, int>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public Task<IActionResult> GetReportStatusCount()
        {
            return ExecuteAsync(async () => Ok(await Service.GetReportStatusCount()));
        }

        [HttpGet("/utmtag")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetUTMTagReport()
        {
            return await ExecuteAsync(async () => Ok(await Service.GetUTMTagReport()));
        }
    }
}
