﻿using CreditExchange.Applications.Filters.Abstractions.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;


namespace CreditExchange.Applications.Filters
{
    public static class ApplicationFilterListenerExtensions
    {
        public static void UseApplicationFilterListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IApplicationFilterListener>().Start();
        }
    }
}
