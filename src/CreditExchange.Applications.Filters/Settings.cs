﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace CreditExchange.Applications.Filters
{
    public class Settings
    {
        public static string ServiceName { get; } = "application-filters";

        public static string TaggingEvents { get; } = "tagging-events";

      
    }
}
