﻿using CreditExchange.Applications.Filters.Abstractions;
using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Date;
using LendFoundry.Configuration.Client;
using CreditExchange.Applications.Filters.Abstractions.Configurations;
using LendFoundry.Clients.DecisionEngine;
using CreditExchange.Applications.Filters.Abstractions.Services;
using LendFoundry.StatusManagement.Client;
using LendFoundry.VerificationEngine.Client;
using LendFoundry.DataAttributes.Client;
using LendFoundry.Security.Identity.Client;
using CreditExchange.Application.Client;
using LendFoundry.Configuration;
using LendFoundry.ProductRule.Client;

namespace CreditExchange.Applications.Filters
{
    public class ApplicationFilterServiceFactory : IApplicationFilterServiceFactory
    {
        public ApplicationFilterServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        public IServiceProvider Provider { get; }

        public IApplicationFilterServiceExtended Create(ITokenReader reader, ITokenHandler handler, ILogger logger)
        {
            var repositoryFactory = Provider.GetService<IFilterViewRepositoryFactory>();
            var repository = repositoryFactory.Create(reader);

            var tagsRepositoryFactory = Provider.GetService<IFilterViewTagsRepositoryFactory>();
            var tagsRepository = tagsRepositoryFactory.Create(reader);

            var eventHubFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHub = eventHubFactory.Create(reader);
            
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var taggingConfigurationService = configurationServiceFactory.Create<TaggingConfiguration>(Settings.TaggingEvents, reader);
            var taggingConfiguration = taggingConfigurationService.Get();

            var configurationService = configurationServiceFactory.Create<Configuration>(Settings.ServiceName, reader);
            var configuration = configurationService.Get();

            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTime = tenantTimeFactory.Create(configurationServiceFactory, reader);

            var decisionEngineFactory = Provider.GetService<IDecisionEngineClientFactory>();
            var decisionEngine = decisionEngineFactory.Create(reader);

            var statusManagementServiceFactory = Provider.GetService<IStatusManagementServiceFactory>();
            var statusService = statusManagementServiceFactory.Create(reader);

            var dataAttributesClientFactory = Provider.GetService<IDataAttributesClientFactory>();
            var dataAttributesEngineService = dataAttributesClientFactory.Create(reader);

            var verficationEngineServiceFactory = Provider.GetService<IVerificationEngineServiceClientFactory>();
            var verificationEngineService = verficationEngineServiceFactory.Create(reader);

            var identityServiceFactory = Provider.GetService<IIdentityServiceFactory>();
            var identityEngineService = identityServiceFactory.Create(reader);

            var applicationSeriveFactory = Provider.GetService<IApplicationServiceClientFactory>();
            var applicationEngineService = applicationSeriveFactory.Create(reader);

            var productRuleSeriveFactory = Provider.GetService<IProductRuleServiceClientFactory>();
            var productRuleService = productRuleSeriveFactory.Create(reader);

            ICsvGenerator csvGenerator = new CsvExporter.CsvGenerator();

            return new ApplicationFilterService(repository, tagsRepository, eventHub, logger, tenantTime, configuration, taggingConfiguration, decisionEngine, statusService, dataAttributesEngineService, verificationEngineService, csvGenerator,
                reader, handler, identityEngineService, applicationEngineService, productRuleService);
        }
    }
}
