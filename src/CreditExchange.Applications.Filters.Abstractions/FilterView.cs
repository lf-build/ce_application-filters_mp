﻿using CreditExchange.Applications.Filters.Abstractions;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using LendFoundry.StatusManagement;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace CreditExchange.Applications.Filters
{
    public class FilterView : Aggregate, IFilterView
    {
        public FilterView()
        {

        }
        public string ApplicationId { get; set; }
        public string ApplicantId { get; set; }
        public string ApplicationNumber { get; set; }
        public string Applicant { get; set; }
        public double AmountRequested { get; set; }
        public string RequestTermType { get; set; }
        public double RequestTermValue { get; set; }
        public string ApplicantFirstName { get; set; }
        public string ApplicantLastName { get; set; }
        public string ApplicantMiddleName { get; set; }
        public DateTimeOffset? ApplicantDateOfBirth { get; set; }
        public string ApplicantPhone { get; set; }
        public string ApplicantWorkEmail { get; set; }
        public string ApplicantPersonalEmail { get; set; }
        public string ApplicantPanNumber { get; set; }
        public string ApplicantAadharNumber { get; set; }
        public string ApplicantEmployer { get; set; }
        public string ApplicantAddressLine1 { get; set; }
        public string ApplicantAddressLine2 { get; set; }
        public string ApplicantAddressLine3 { get; set; }
        public string ApplicantAddressLine4 { get; set; }
        public string ApplicantAddressCity { get; set; }
        public string ApplicantAddressCountry { get; set; }
        public bool ApplicantAddressIsDefault { get; set; }
        public DateTimeOffset Submitted { get; set; }
        public DateTimeOffset ExpirationDate { get; set; }
        public DateTimeOffset LastProgressDate { get; set; }

        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public DateTimeOffset StatusDate { get; set; }
        public List<string> StatusReasons { get; set; }

        public string SourceId { get; set; }
        public string SourceType { get; set; }

        public string FileType { get; set; }
        public double InitialOfferAmount { get; set; }
        public double InitialOfferInterestRate { get; set; }        
        public int InitialOfferLoanTenure { get; set; }
        public double InitialOfferScore { get; set; }
        public DateTimeOffset? InitialOfferSelectedOn { get; set; }
                
        public double FinalOfferAmount { get; set; }
        public double FinalOfferInterestRate { get; set; }
        public double FinalOfferInstallmentAmount { get; set; }
        public string FinalOfferLoanTenure { get; set; }
        public double FinalOfferProcessingFee { get; set; }
        public DateTimeOffset? FinalOfferSelectedOn { get; set; }
        
        public Dictionary<string, object> StatusHistory { get; set; }

        public double DisbursedAmount { get; set; }

        public DateTimeOffset DisbursementDate { get; set; }

        public double EMIAmount { get; set; }

        public DateTimeOffset FirstEMIDate { get; set; }

        public string FinacleDocumentId { get; set; }
        public string FinacleDocEntityId { get; set; }

        public string HunterDocumentId { get; set; }
        public string HunterDocEntityId { get; set; }

        public DateTime? CanReapplyBy { get; set; }

        public DateTimeOffset? OfferRejectedDate { get; set; }
        public DateTimeOffset? OfferExpiredDate { get; set; }
        public DateTimeOffset? ApprovedDate { get; set; }
        public DateTimeOffset? NotInterestedDate { get; set; }

        public Dictionary<string, string> Assignees { get; set; }

        public string PromoCode { get; set; }
        public string TrackingCode { get; set; }
        public bool? HunterStatus { get; set; }
        public string SodexoCode { get; set; }
        public string TrackingCodeMedium { get; set; }
        public string HunterId { get; set; }
        public string SchemeCode { get; set; }
        public double? Income { get; set; }
        public string ApplicantAddressPincode { get; set; }
        public IEnumerable<TagInfo> Tags { get; set; }
        public string LeadSquaredCRMId { get; set; }
        public string SystemChannel { get; set; }
        public string GCLId { get; set; }
        public string LCaseApplicant { get; set; }
        public string LCaseApplicantEmployer { get; set; }
        public string LCaseApplicantPersonalEmail { get; set; }
        public string LCaseApplicantPanNumber { get; set; }
        public string ProductId { get; set; }
        public string OldProductId { get; set; }
        public string StatusWorkFlowId { get; set; }
        public WorkFlowStatus WorkFlowStatus { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ISubStatus, SubStatus>))]
        public List<ISubStatus> SubStatusDetail { get; set; }

        public string TrackingCodeCampaign { get; set; }
        public string TrackingCodeTerm { get; set; }
        public string TrackingCodeContent { get; set; }

    }
}