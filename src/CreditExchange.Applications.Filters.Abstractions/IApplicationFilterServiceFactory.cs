﻿using CreditExchange.Applications.Filters.Abstractions.Services;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace CreditExchange.Applications.Filters.Abstractions
{
    public interface IApplicationFilterServiceFactory
    {
        IApplicationFilterServiceExtended Create(ITokenReader reader,ITokenHandler tokenHandler, ILogger logger);
    }
}
