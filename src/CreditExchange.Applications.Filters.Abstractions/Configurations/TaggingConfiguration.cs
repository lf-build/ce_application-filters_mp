﻿using System.Collections.Generic;

namespace CreditExchange.Applications.Filters.Abstractions.Configurations
{
    public class TaggingConfiguration
    {
        public List<TaggingEvent> Events { get; set; }
    }
}
