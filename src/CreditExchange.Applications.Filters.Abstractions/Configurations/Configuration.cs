﻿using System.Collections.Generic;

namespace CreditExchange.Applications.Filters.Abstractions.Configurations
{
    public class Configuration
    {
        public EventMapping[] Events { get; set; }
        public Dictionary<string, Rule> Tags { get; set; }
        public string[] ApprovedStatuses { get; set; }
        public string[] ReApplyStatuseCodes { get; set; }
        public int ReApplyTimeFrameDays { get; set; }
        public string UpdateRejectionDateStatus { get; set; }
        public string UpdateExpiredDateStatus { get; set; }
        public string UpdateApprovedDateStatus { get; set; }
        public string UpdateNotInterestedDateStatus { get; set; }

        public IEnumerable<ExportTemplateDetails> ExportTemplates { get; set; }

        public IEnumerable<ExportUtmTagData> ExportUtmTagData { get; set; }
        public string[] BankPortalStatusCodes { get; set; }
    }
}
