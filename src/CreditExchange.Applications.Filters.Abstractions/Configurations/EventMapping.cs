﻿namespace CreditExchange.Applications.Filters.Abstractions.Configurations
{
    public class EventMapping
    {
        public string Name { get; set; }
        public string ApplicationNumber { get; set; }
        public bool UpdateLastProgressDate { get; set; }
        public bool ShouldUpdateInitialOfferSelectedDate { get; set; }
        public bool ShouldUpdateFinalOfferSelectedDate { get; set; }
        public bool ShouldUpdateAppFilterRecord { get; set; }
    }
}
