﻿using System.Collections.Generic;

namespace CreditExchange.Applications.Filters.Abstractions
{
    public class RuleResult
    {
        public List<string> ApplyTags { get; set; }
        public List<string> RemoveTags { get; set; }
    }
}
