﻿namespace CreditExchange.Applications.Filters.Abstractions
{
    public enum UniqueParameterTypes
    {
        Pan,
        Email,
        Mobile
    }
}
