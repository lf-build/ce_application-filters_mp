﻿
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CreditExchange.Applications.Filters.Abstractions.Services
{
    public interface IApplicationFilterService
    {
        IEnumerable<IFilterView> GetAll();
        IEnumerable<IFilterView> GetAllBySource(string sourceType, string sourceId);

        IEnumerable<IFilterView> GetBySourceAndStatus(string sourceType, string sourceId, IEnumerable<string> statuses);

        Task<int> GetTotalSubmittedForCurrentMonth(string sourceType, string sourceId);

        Task<int> GetTotalSubmittedForCurrentYear(string sourceType, string sourceId);

        Task<IEnumerable<IFilterView>> GetByName(string sourceType, string sourceId, string name);

        Task<IEnumerable<IFilterView>> GetByStatus(string sourceType, string sourceId, IEnumerable<string> statuses);

        Task<IEnumerable<IFilterView>> GetAllByStatus(IEnumerable<string> statuses);

        Task<int> GetCountByStatus(string sourceType, string sourceId, IEnumerable<string> statuses);
                
        Task<IEnumerable<IFilterView>> GetAllByTag(IEnumerable<string> tags);

        Task<IEnumerable<IFilterViewTags>> GetTagInfoForApplicationsByTag(string tag);

        Task<int> GetCountByTag(IEnumerable<string> tags);

        double GetTotalAmountApprovedBySource(string sourceType, string sourceId);

        Task<IFilterView> GetByApplicationNumber(string sourceType, string sourceId, string applicationNumber);

        Task<IFilterView> GetByApplicationNumberWithStatusHistory(string sourceType, string sourceId, string applicationNumber);

        Task<IEnumerable<IFilterView>> DuplicateExistsData(UniqueParameterTypes parameterName, string parameterValue);

        Task<IEnumerable<IFilterView>> GetAllExpiredApplications();

        void TagsWithoutEvaluation(string applicationNumber, IEnumerable<string> tags);

        void UnTagWithoutEvaluation(string applicationNumber, IEnumerable<string> tags);

        Task<IFundingData> GetDataAttributeForFunding(string entityType, string entityId);
        Task<IEnumerable<IFilterView>> SearchApplications(ApplicationSearchParams searchParams, string condition = null);
        Tuple<string, FilterView, FilterViewTags> SearchApplicationForAttribute(string attributeName, string attributeValue);
        IFilterViewTags GetApplicationTagInformation(string applicationNumber);
        FileContentResult ExportAll(string templateName);
        Task<FileContentResult> ExportAllByStatus(string templateName, IEnumerable<string> statuses);
        Task<FileContentResult> ExportAllByTag(string templateName, IEnumerable<string> tags);
        void AttachFinacleDocumentID(string applicationNumber, string docManagerEntityId, string finacleDocumentId);
        void AttachHunterDocumentID(string applicationNumber, string docManagerEntityId, string hunterDocumentId);
        Task<IEnumerable<IFilterView>> ApplicationsTaggedOn(string tagName, string fromDate, string toDate = null);

        Task<IEnumerable<IFilterView>> GetAllAssignedApplications();

        Task<IEnumerable<IFilterView>> GetAllUnassigned();

        Task<IEnumerable<IFilterView>> GetAllUnassignedByUser();

        Task<IEnumerable<IFilterView>> SearchApplicationsForBank(ApplicationSearchParams searchParams, string condition = null);
        Task<IDictionary<string, int>> GetReportStatusCount();
        Task<IEnumerable<IFilterView>> GetUTMTagReport();
        Task<IEnumerable<IFilterView>> GetExcludedStatusExpired(IEnumerable<string> excludedStatuses);

    }
}
