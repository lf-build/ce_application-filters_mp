﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CreditExchange.Applications.Filters.Abstractions.Services
{
    public interface IFilterViewRepository : IRepository<IFilterView>
    {
        IEnumerable<IFilterView> GetAll();
        void AddOrUpdate(IFilterView view);
        IEnumerable<IFilterView> GetAllBySource(string sourceType, string sourceId);

        IEnumerable<IFilterView> GetBySourceAndStatus(string sourceType, string sourceId, IEnumerable<string> statuses);

        Task<IEnumerable<IFilterView>> GetAllByStatus(IEnumerable<string> statuses);

        Task<int> GetTotalSubmittedForCurrentMonth(string sourceType, string sourceId, DateTimeOffset currentMonth);

        Task<int> GetTotalSubmittedForCurrentYear(string sourceType, string sourceId, DateTimeOffset currentYear);

        Task<IEnumerable<IFilterView>> GetByName(string sourceType, string sourceId, string name);

        Task<IEnumerable<IFilterView>> GetByStatus(string sourceType, string sourceId, IEnumerable<string> statuses);

        Task<int> GetCountByStatus(string sourceType, string sourceId, IEnumerable<string> statuses);

        double GetTotalAmountApprovedBySourceAndStatus(string sourceType, string sourceId, string[] statuses);

        Task<IFilterView> GetByApplicationNumber(string sourceType, string sourceId, string applicationNumber);

        Task<IFilterView> GetByApplicationNumber(string applicationNumber);

        Task<IEnumerable<IFilterView>> GetUsingUniqueAttributes(UniqueParameterTypes parameterName, string parameterValue);

        Task<IEnumerable<IFilterView>> GetAllExpiredApplications(DateTimeOffset todayDate);

        IEnumerable<IFilterView> SearchApplications(ApplicationSearchParams searchParams, string condition = null);

        IEnumerable<IFilterView> SearchApplicationForAttribute(string attributeName, string attributeValue);
        void AttachFinacleDocumentId(string applicationNumber, string docManagerEntityId, string documentId);
        void AttachHunterDocumentId(string applicationNumber, string docManagerEntityId, string documentId);

        Task<IEnumerable<IFilterView>> GetAllUnassigned();

        Task<IEnumerable<IFilterView>> GetAllAssignedApplications(string userName);

        Task<IEnumerable<IFilterView>> GetAllUnassignedByUser(IEnumerable<string> roles);

        Task<IDictionary<string, int>> GetReportStatusCount();

        IEnumerable<IFilterView> GetUTMTagReport(IEnumerable<string> applicationnumber);

        Task<IEnumerable<IFilterView>> GetAllExpiredApplications(DateTimeOffset todayDate, List<string> excludedStatuses);
    }
}