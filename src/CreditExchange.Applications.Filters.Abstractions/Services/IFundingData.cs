﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Applications.Filters.Abstractions.Services
{
   public interface IFundingData
    {
        double FundedAmount { get; set; }
        DateTimeOffset FundedDate { get; set; }
        double EmiAmount { get; set; }
        DateTimeOffset FirstEmiDate { get; set; }
    }
}
