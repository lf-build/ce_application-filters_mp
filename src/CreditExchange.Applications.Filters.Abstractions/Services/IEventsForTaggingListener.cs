﻿namespace CreditExchange.Applications.Filters.Abstractions.Services
{
    public interface IEventsForTaggingListener
    {
        void Start();
    }
}
