﻿using LendFoundry.Security.Tokens;

namespace CreditExchange.Applications.Filters.Abstractions.Services
{
    public interface IFilterViewTagsRepositoryFactory
    {
        IFilterViewTagsRepository Create(ITokenReader reader);
    }
}
