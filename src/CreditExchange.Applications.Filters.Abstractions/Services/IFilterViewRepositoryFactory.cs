﻿using LendFoundry.Security.Tokens;

namespace CreditExchange.Applications.Filters.Abstractions.Services
{
    public interface IFilterViewRepositoryFactory
    {
        IFilterViewRepository Create(ITokenReader reader);
    }
}
