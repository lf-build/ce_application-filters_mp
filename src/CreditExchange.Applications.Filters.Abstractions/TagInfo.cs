﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Applications.Filters.Abstractions
{
    public class TagInfo
    {
        public string TagName { get; set; }
        public DateTimeOffset TaggedOn { get; set; }
    }
}
