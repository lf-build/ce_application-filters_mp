﻿using CreditExchange.Applications.Filters.Abstractions;
using CreditExchange.Applications.Filters.Abstractions.Services;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Services;
using RestSharp;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace CreditExchange.Applications.Filters.Client
{
    public class ApplicationFilterClientService : IApplicationFilterService
    {
        public ApplicationFilterClientService(IServiceClient client) { Client = client; }

        private IServiceClient Client { get; }

        public IEnumerable<IFilterView> GetAll()
        {
            var request = new RestRequest("/all", Method.GET);
            return Client.Execute<List<FilterView>>(request);
        }

        public IEnumerable<IFilterView> GetAllBySource(string sourceType, string sourceId)
        {
            Validade(sourceType, sourceId);

            var request = new RestRequest("/{sourceType}/{sourceId}/all", Method.GET);
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);

            return Client.Execute<List<FilterView>>(request);
        }

        public IEnumerable<IFilterView> GetBySourceAndStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            Validade(sourceType, sourceId);

            var request = new RestRequest("/{sourceType}/{sourceId}/status", Method.GET);
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);
            AppendItems(request, statuses.ToList(), "statuses");

            return Client.Execute<List<FilterView>>(request);
        }

        public async Task<int> GetTotalSubmittedForCurrentMonth(string sourceType, string sourceId)
        {
            Validade(sourceType, sourceId);

            var request = new RestRequest("/{sourceType}/{sourceId}/metrics/submit/month", Method.GET);
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);

            return await Client.ExecuteAsync<int>(request);            
        }

        public async Task<int> GetTotalSubmittedForCurrentYear(string sourceType, string sourceId)
        {
            Validade(sourceType, sourceId);

            var request = new RestRequest("/{sourceType}/{sourceId}/metrics/submit/year", Method.GET);
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);

            return await Client.ExecuteAsync<int>(request);            
        }

        public async Task<IEnumerable<IFilterView>> GetByName(string sourceType, string sourceId, string name)
        {
            Validade(sourceType, sourceId);

            if (string.IsNullOrWhiteSpace(name))
                throw new InvalidArgumentException($"{nameof(name)} is mandatory", nameof(name));

            var request = new RestRequest("/{sourceType}/{sourceId}/search/{name}", Method.GET);
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);
            request.AddUrlSegment(nameof(name), name);

            return await Client.ExecuteAsync<List<FilterView>>(request);
        }

        public async Task<IEnumerable<IFilterView>> GetByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            Validade(sourceType, sourceId);
            var request = new RestRequest("/{sourceType}/{sourceId}/status", Method.GET);
                        
            request.AddUrlSegment("sourceType", sourceType);
            request.AddUrlSegment("sourceId", sourceId);
            AppendItems(request, statuses.ToList(), "statuses");

            return await Client.ExecuteAsync<List<FilterView>>(request);
        }

        public async Task<int> GetCountByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            Validade(sourceType, sourceId);
            var request = new RestRequest("/{sourceType}/{sourceId}/count/status", Method.GET);
            
            request.AddUrlSegment("sourceType", sourceType);
            request.AddUrlSegment("sourceId", sourceId);
            AppendItems(request, statuses.ToList(), "statuses");

            return await Client.ExecuteAsync<int>(request);
        }

        public async Task<IEnumerable<IFilterView>> GetByTag(string sourceType, string sourceId, IEnumerable<string> tags)
        {
            Validade(sourceType, sourceId);
            var request = new RestRequest("/{sourceType}/{sourceId}/tag", Method.GET);            
            request.AddUrlSegment("sourceType", sourceType);
            request.AddUrlSegment("sourceId", sourceId);
            AppendItems(request, tags.ToList(), "tags");
            return await Client.ExecuteAsync<List<FilterView>>(request);
        }

        public async Task<int> GetCountByTag(IEnumerable<string> tags)
        {
            var request = new RestRequest("/count/tag", Method.GET);
            AppendItems(request, tags.ToList(), "tags");
            return await Client.ExecuteAsync<int>(request);
        }

        public double GetTotalAmountApprovedBySource(string sourceType, string sourceId)
        {
            var request = new RestRequest("/{sourceType}/{sourceId}/metrics/total-approved-amount", Method.GET);
            request.AddUrlSegment("sourceType", sourceType);
            request.AddUrlSegment("sourceId", sourceId);
            return Client.Execute<double>(request);
            //if (result == null)
            //    return 0;

            //return Convert.ToDouble(result);
        }

        public async Task<IFilterView> GetByApplicationNumber(string sourceType, string sourceId, string applicationNumber)
        {
            Validade(sourceType, sourceId);

            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new InvalidArgumentException($"{nameof(applicationNumber)} is mandatory", nameof(applicationNumber));

            var request = new RestRequest("/{sourceType}/{sourceId}/{applicationNumber}", Method.GET);
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);
            request.AddUrlSegment(nameof(applicationNumber), applicationNumber);

            return await Client.ExecuteAsync<FilterView>(request);
        }

        public async Task<IEnumerable<IFilterView>> GetAllExpiredApplications()
        {
            var request = new RestRequest("/expired/all", Method.GET); 
            return await Client.ExecuteAsync<List<FilterView>>(request);
        }

        public async Task<IEnumerable<IFilterView>> GetExcludedStatusExpired(IEnumerable<string> excludedStatuses)
        {
            var request = new RestRequest("/expired/by", Method.GET);
            AppendItems(request, excludedStatuses.ToList(), "statusCode");
            return await Client.ExecuteAsync<List<FilterView>>(request);
        }

        private void Validade(string sourceType, string sourceId)
        {
            if (string.IsNullOrWhiteSpace(sourceType))
                throw new InvalidArgumentException($"{nameof(sourceType)} is mandatory", nameof(sourceType));

            if (string.IsNullOrWhiteSpace(sourceId))
                throw new InvalidArgumentException($"{nameof(sourceId)} is mandatory", nameof(sourceId));
        }

        public async Task<IEnumerable<IFilterView>> DuplicateExistsData(UniqueParameterTypes parameterType, string parameterValue)
        {
            try
            {
                var request = new RestRequest("duplicateexistsdata/{parametertype}/{parametervalue}", Method.GET);
                request.AddUrlSegment("parametertype", parameterType.ToString());
                request.AddUrlSegment("parametervalue", parameterValue);

                return await Client.ExecuteAsync<IEnumerable<FilterView>>(request);
            }catch(Exception ex)
            {
                throw ex;
            }
        }

        public async Task<IEnumerable<IFilterView>> GetAllByStatus(IEnumerable<string> statuses)
        {
            var request = new RestRequest("status", Method.GET);
            AppendItems(request, statuses.ToList(), "statuses");

            return await Client.ExecuteAsync<List<FilterView>>(request);
        }

        public void TagsWithoutEvaluation(string applicationNumber, IEnumerable<string> tags)
        {
            var request = new RestRequest("applytags/{applicationnumber}", Method.POST);
            request.AddUrlSegment("applicationnumber", applicationNumber);
            AppendItems(request, tags.ToList(), "tags");
            Client.Execute(request);
        }

        public void UnTagWithoutEvaluation(string applicationNumber, IEnumerable<string> tags)
        {
            var request = new RestRequest("removetags/{applicationnumber}", Method.POST);
            request.AddUrlSegment("applicationnumber", applicationNumber);
            AppendItems(request, tags.ToList(), "tags");
            Client.Execute(request);
        }

        public async Task<IFilterView> GetByApplicationNumberWithStatusHistory(string sourceType, string sourceId, string applicationNumber)
        {            
            var request = new RestRequest("getwithstatushistory/{sourceType}/{sourceId}/{applicationNumber}", Method.GET);
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);
            request.AddUrlSegment(nameof(applicationNumber), applicationNumber);

            return await Client.ExecuteAsync<IFilterView>(request);
        }

        public async Task<IEnumerable<IFilterView>> GetAllByTag(IEnumerable<string> tags)
        {
            var request = new RestRequest("tag", Method.GET);
            AppendItems(request, tags.ToList(), "tags");

            return await Client.ExecuteAsync<List<FilterView>>(request);
        }

        public async Task<IEnumerable<IFilterViewTags>> GetTagInfoForApplicationsByTag(string tag)
        {
            var request = new RestRequest("tag/taginfo/{tag}", Method.GET);            
            request.AddUrlSegment("tag", tag);

            return await Client.ExecuteAsync<IEnumerable<FilterViewTags>>(request);
        }

        private static void AppendItems(IRestRequest request, IReadOnlyList<string> items, string itemType)
        {
            if (items == null || !items.Any())
                return;

            var url = request.Resource;
            for (var index = 0; index < items.Count; index++)
            {
                var itemValue = items[index];
                url = url + $"/{{{itemType}{index}}}";
                request.AddUrlSegment($"{itemType}{index}", itemValue);
            }
            request.Resource = url;
        }

        public Task<IFundingData> GetDataAttributeForFunding(string entityType, string entityId)
        {
            throw new NotImplementedException();
        }

        

        public Tuple<string, FilterView, FilterViewTags> SearchApplicationForAttribute(string attributeName, string attributeValue)
        {
            var request = new RestRequest("searchforattribute/{attributename}/{attributevalue}", Method.GET);
            request.AddUrlSegment("attributename", attributeName);
            request.AddUrlSegment("attributevalue", attributeValue);

            return Client.Execute<Tuple<string, FilterView, FilterViewTags>>(request);
        }

        public IFilterViewTags GetApplicationTagInformation(string applicationNumber)
        {
            throw new NotImplementedException();
        }

        public FileContentResult ExportAll(string templateName)
        {
            throw new NotImplementedException();
        }

        public Task<FileContentResult> ExportAllByStatus(string templateName, IEnumerable<string> statuses)
        {
            throw new NotImplementedException();
        }

        public Task<FileContentResult> ExportAllByTag(string templateName, IEnumerable<string> tags)
        {
            throw new NotImplementedException();
        }

        public void AttachFinacleDocumentID(string applicationNumber, string docManagerEntityId, string finacleDocumentId)
        {
            var request = new RestRequest("attachfinacleDoc/{docmanagerentityid}/{applicationnumber}/{documentid}", Method.POST);
            request.AddUrlSegment("docmanagerentityid", docManagerEntityId);
            request.AddUrlSegment("applicationnumber", applicationNumber);
            request.AddUrlSegment("documentid", finacleDocumentId);

            Client.Execute(request);
        }

        public void AttachHunterDocumentID(string applicationNumber, string docManagerEntityId, string hunterDocumentId)
        {
            var request = new RestRequest("attachhunterDoc/{docmanagerentityid}/{applicationnumber}/{documentid}", Method.POST);
            request.AddUrlSegment("docmanagerentityid", docManagerEntityId);
            request.AddUrlSegment("applicationnumber", applicationNumber);
            request.AddUrlSegment("documentid", hunterDocumentId);

            Client.Execute(request);
        }

        public Task<IEnumerable<IFilterView>> ApplicationsTaggedOn(string tagName, string fromDate, string toDate = null)
        {
            throw new NotImplementedException();
        }
                
        public Task<IEnumerable<IFilterView>> SearchApplications(ApplicationSearchParams searchParams, string condition = null)
        {
            throw new NotImplementedException();
        }
       public Task<IEnumerable<IFilterView>> SearchApplicationsForBank(ApplicationSearchParams searchParams, string condition = null)
        {
            throw new NotImplementedException();
        }
        public Task<IEnumerable<IFilterView>> GetAllUnassigned()
        {
            var request = new RestRequest("/unassigned/all", Method.GET);
            return Client.ExecuteAsync<IEnumerable<IFilterView>>(request);
        }

        public Task<IEnumerable<IFilterView>> GetAllAssignedApplications()
        {
            var request = new RestRequest("/mine", Method.GET);
            return Client.ExecuteAsync<IEnumerable<IFilterView>>(request);
        }

        public Task<IEnumerable<IFilterView>> GetAllUnassignedByUser()
        {
            throw new NotImplementedException();
        }

        public async Task<IDictionary<string,int>> GetReportStatusCount()
        {
            var request = new RestRequest("/count", Method.GET);
            return await Client.ExecuteAsync<Dictionary<string, int>>(request);
        }

        public async Task<IEnumerable<IFilterView>> GetUTMTagReport()
        {
            var request = new RestRequest("/utmtag", Method.GET);
            var data = await Client.ExecuteAsync<IEnumerable<FilterView>>(request);
            return data.AsEnumerable<IFilterView>();
        }
     
    }
}
